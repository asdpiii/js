const coin = document.querySelector('#coin');
const button = document.querySelector('#flip');
const heads = document.querySelector('#heads');
const tails = document.querySelector('#tails');




function deferFn(callback, ms) {
  setTimeout(callback, ms); 
}


function processResult(result) {
   if (result === 'heads') {
       var inputVal = document.getElementById("decide1").value;
    alert(inputVal + " Yes! Do it!");
    } else {
        alert(inputVal + " No! Don't do it!");
    }
}

function flipCoin() {
  coin.setAttribute('class', '');
  const random = Math.random();
  const result = random < 0.5 ? 'heads' : 'tails';
 deferFn(function() {
   coin.setAttribute('class', 'animate-' + result);
   deferFn(processResult.bind(null, result), 2900);
 }, 100);
}

button.addEventListener('click', flipCoin);


